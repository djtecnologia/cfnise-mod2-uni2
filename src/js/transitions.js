Vue.transition('fadeUpDown', {
  enterClass: 'fadeInDownBig',
  leaveClass: 'fadeOutUpBig'
});

Vue.transition('fadeDj', {
  enterClass: 'fadeIn',
  leaveClass: 'fadeOut'
});

Vue.transition('paper', {
  enterClass: 'slideInUp',
  leaveClass: 'rotateOutUpLeft',
  mode: 'out-in'
});