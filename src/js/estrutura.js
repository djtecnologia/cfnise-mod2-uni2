var entradaDefault = 'fadeIn';
var saidaDefault = 'fadeOut';

var estrutura = [
  {
    title: 'Disciplina 1',
    idf: 'disciplina1',
    shortTitle: '<i class="fa fa-book"></i> 2',
    paginacao: true,
    porcentagem: true,
    datelib: [
      {
        dia: 7,
        mes: 2,
        ano: 2016
      }
    ],
    paginas: [

      /*TELA 1*/{file: "pag1.html", title: "Template: T001"},
      /*TELA 2*/{file: "pag2.html", title: "Template: T002"},
      /*TELA 3*/{file: "pag3.html", title: "Template: T032.004"},
      /*TELA 4*/{file: "pag4.html", title: "Template: T051"},
      /*TELA 5*/{file: "pag5.html", title: "Template: T051.001"},
      /*TELA 6*/{file: "pag6.html", title: "Template: T002"},
      /*TELA 7*/{file: "pag7.html", title: "Template: T030"},
      /*TELA 8*/{file: "pag8.html", title: "Template: T005.034"},
      /*TELA 9*/{file: "pag9.html", title: "Template: T030.001"},
      /*TELA 10*/{file: "pag10.html", title: "Template: T051.002"},
      /*TELA 11*/{file: "pag11.html", title: "Template: T030.003"},
      /*TELA 12*/{file: "pag12.html", title: "Template: T051.002"},
      /*TELA 13*/{file: "pag13.html", title: "Template: T030.003"},// setas e destaque piscam
      /*TELA 14*/{file: "pag14.html", title: "Template: T030.003"},// setas e destaque piscam
      /*TELA 15*/{file: "pag15.html", title: "Template: T002"},
      /*TELA 16*/{file: "pag16.html", title: "Template: T0030"},// setas e destaque piscam
      /*TELA 17*/{file: "pag17.html", title: "Template: T005_002"},
      /*TELA 18*/{file: "pag18.html", title: "Template: T030.002"},
      /*TELA 19*/{file: "pag19.html", title: "Template: T051.006"},
      /*TELA 20*/{file: "pag20.html", title: "Template: T051.007"},
      /*TELA 21*/{file: "pag21.html", title: "Template: T027.006"},
     // /*TELA 22*/{file: "pag22.html", title: "Template: T027.006"},
      /*TELA 23*/{file: "pag23.html", title: "Template: T027.006"},
      /*TELA 24*/{file: "pag24.html", title: "Template: T027.006"},
      /*TELA 25*/{file: "pag25.html", title: "Template: T027.006"},
      /*TELA 26*/{file: "pag26.html", title: "Template: T027.006"},
      /*TELA 27*/{file: "pag27.html", title: "Template: T027.006"},
      /*TELA 27*/{file: "pag27a.html", title: "Template: T027.006"},
      /*TELA 29*/{file: "pag29.html", title: "Template: T027.006"},
      /*TELA 30*/{file: "pag30.html", title: "Template: T027.006"},
      /*TELA 31*/{file: "pag31.html", title: "Template: T027.006"},
      /*TELA 32*/{file: "pag32.html", title: "Template: T027.006"},
      /*TELA 33*/{file: "pag33.html", title: "Template: T027.006"},
      /*TELA 34*/{file: "pag34.html", title: "Template: T027.006"},
      /*TELA 35*/{file: "pag35.html", title: "Template: T027.006"},
      /*TELA 36*/{file: "pag36.html", title: "Template: T027.006"},
      /*TELA 37*/{file: "pag37.html", title: "Template: T027.006"},
      /*TELA 38*/{file: "pag38.html", title: "Template: T027.006"},
      /*TELA 39*/{file: "pag39.html", title: "Template: T027.006"},
      /*TELA 40*/{file: "pag40.html", title: "Template: T027.006"},
      /*TELA 41*/{file: "pag41.html", title: "Template: T027.006"},
      /*TELA 42*/{file: "pag42.html", title: "Template: T051.004"},
      /*TELA 43*/{file: "pag43.html", title: "Template: T027.006"},// setas e destaque piscam
      /*TELA 44*/{file: "pag44.html", title: "Template: T027.006"},
      /*TELA 45*/{file: "pag45.html", title: "Template: T027.006"},
      /*TELA 46*/{file: "pag46.html", title: "Template: T027.006"},
      /*TELA 47*/{file: "pag47.html", title: "Template: T002"},
      /*TELA 48*/{file: "pag48.html", title: "Template: T002"},
      /*TELA 49*/{file: "pag49.html", title: "Template: T002"},
      /*TELA 50*/{file: "pag50.html", title: "Template: T002"},
      /*TELA 51*/{file: "pag51.html", title: "Template: T002"},//resolv 51 - 69
      /*TELA 52*/{file: "pag52.html", title: "Template: T002"},
      /*TELA 53*/{file: "pag53.html", title: "Template: T002"},
      /*TELA 54*/{file: "pag54.html", title: "Template: T002"},
      /*TELA 55*/{file: "pag55.html", title: "Template: T002"},
      /*TELA 56*/{file: "pag56.html", title: "Template: T002"},
      /*TELA 57*/{file: "pag57.html", title: "Template: T002"},
      /*TELA 58*/{file: "pag58.html", title: "Template: T002"}, //negritos
      /*TELA 59*/{file: "pag59.html", title: "Template: T051.005"},
      /*TELA 60*/{file: "pag60.html", title: "Template: T002"},
      /*TELA 61*/{file: "pag61.html", title: "Template: T002"},
      /*TELA 62*/{file: "pag62.html", title: "Template: T002"},
      /*TELA 63*/{file: "pag63.html", title: "Template: T002"},
      /*TELA 64*/{file: "pag64.html", title: "Template: T002"},
      /*TELA 65*/{file: "pag65.html", title: "Template: T002"},
      /*TELA 66*/{file: "pag66.html", title: "Template: T002"},
      /*TELA 67*/{file: "pag67.html", title: "Template: T002"},
      /*TELA 68*/{file: "pag68.html", title: "Template: T002"},
      /*TELA 69*/{file: "pag69.html", title: "Template: T036"},
      /*TELA 73*/{file: "pag73.html", title: "Template: T002"},
    ]
  },
];
