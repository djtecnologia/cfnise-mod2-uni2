var mapa = {
  name: 'Curso de Formação em Nível Inicial para Secretários Escolares',
  selected: true,
  unidade_ind: 0,
  range: [0, 69],
  children: [
    {
      name: 'Módulo 2 - Unidade 2 - A Língua Portuguesa e a Comunicação Oficial',
      selected: false,
      unidade_ind: 0,
      range: [0, 69],
      children: [
        {
          name: 'Apresentação',
          selected: false,
          unidade_ind: 0,
          range: [0, 1]
        },
        {
          name: 'A Comunicação Oficial',
          selected: false,
          unidade_ind: 0,
          range: [2, 31]
        },
        {
          name: 'Reforma Ortográfica da Língua Portuguesa',
          selected: false,
          unidade_ind: 0,
          range: [32, 57]
        },
        {
          name: 'Ortografia – Esquemas',
          selected: false,
          unidade_ind: 0,
          range: [58, 65]
        },
        {
          name: 'Atividade',
          selected: false,
          unidade_ind: 0,
          range: [66, 67]
        },
        {
          name: 'Encerramento',
          selected: false,
          unidade_ind: 0,
          range: [68 , 68]
        },

      ]
    }
  ]
};

// define the item component
Vue.component('item', {
  template: '#item-template',
  props: {
    model: Object
  },
  data: function () {
    return {
      open: false
    }
  },
  computed: {
    isFolder: function () {
      return this.model.children &&
        this.model.children.length
    },
    selected: function () {
      return this.model.selected;
    },
    open: function () {
      return this.model.selected === true;
    }
    //  if (this.isFolder) {
    //    sel = false;
    //    this.model.children.forEach(function (element, index, array) {
    //      if (element.name.toLowerCase.indexOf(App.unidade.toLowerCase()) !== -1) {
    //        sel = true;
    //      }
    //    });
    //    return sel;
    //
    //  } else {
    //    return this.model.name.toLowerCase.indexOf(App.unidade.toLowerCase()) !== -1
    //  }
    //}
  },
  watch: {
    model: {
      handler: function (val, oldVal) {
        console.log('a thing changed')
      },
      deep: true
    }
  },
  methods: {
    toggle: function () {
      if (this.isFolder) {
        if (this.model.selected === true) {
          this.model.selected = false;
        } else {
          this.model.selected = true;
        }
      } else {
        if (App.pagina_last > this.model.range[0]) {
          App.goTo(this.model.unidade_ind, this.model.range[0]);
        }
        App.showLeft = false;
      }
    },
    changeType: function () {
      if (!this.isFolder) {
        Vue.set(this.model, 'children', [])
        this.addChild();
        this.open = true
      }
    },
    addChild: function () {
      //this.model.children.push({
      //  name: 'new stuff'
      //})
    }

  }
});