urlObj = urlObject();
var SCORM_ACTIVE = false;
if (urlObj.parameters.scorm) {
  SCORM_ACTIVE = true;
}

function complete() {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    SDD.SetPassed();
  }
}

function GetBookmark() {
  // carga comum
  // clique no sco
  //

  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    var bookmark = SDD.GetBookmark();
    bookmark = bookmark || false;
    if (bookmark) {
      bk = bookmark.split('|');
      return {
        unidade: parseInt(bk[0]),
        pagina: parseInt(bk[1])
      };
    }
  }
  var pagina = 0;
  var unidade = 0;
  if (urlObj.parameters.pag) {
    pagina = urlObj.parameters.pag - 1;
  }
  if (urlObj.parameters.und) {
    unidade = urlObj.parameters.und - 1;
  }
  return {
    unidade: unidade,
    pagina: pagina
  };

}

function SetBookmark(unidade_ind, pagina_ind) {
  if (SCORM_ACTIVE) {
    var SD = window.parent, loc = window.location.href;

    SD.SetBookmark(unidade_ind + '|' + pagina_ind, document.title);

    SD.CommitData();
  }
}

function carregaContraste() {
  var suspend_data = get_suspend_data();
  contraste = 0;
  if ('contraste' in suspend_data) {
    contraste = suspend_data['contraste'];
  }
  if (contraste == 1) {
    return true;
  }
  return false;
}

function salvaContraste(contraste) {
  var suspend_data = get_suspend_data();
  suspend_data['contraste'] = 0;
  if (contraste) {
    suspend_data['contraste'] = 1;
  }
  set_suspend_data(suspend_data);
}

function atualizaPorcentagem(unidade_ind, pagina_ind) {
  if (SCORM_ACTIVE) {
    var suspend_data = get_suspend_data();
    if (unidade_ind in suspend_data) {
      unidade = suspend_data[unidade_ind];
      if (unidade < pagina_ind) {
        suspend_data[unidade_ind] = pagina_ind;
      }
    } else {
      suspend_data[unidade_ind] = pagina_ind;
    }
    set_suspend_data(suspend_data);
    return suspend_data[unidade_ind];
  } else {
    return pagina_ind;
  }
}

function get_suspend_data() {
  if (SCORM_ACTIVE) {

    var SDD = window.parent;
    var suspend_data = SDD.GetDataChunk();
    if (suspend_data === 'null' || suspend_data === '' || suspend_data === null || suspend_data === undefined) {
      return {};
    } else {
      return JSON.parse(suspend_data);
    }
  }
  return {};
}

function set_suspend_data(suspend_data) {
  if (SCORM_ACTIVE) {

    var SDD = window.parent;

    suspend_data = JSON.stringify(suspend_data);
    SDD.SetDataChunk(suspend_data);
    SDD.CommitData();
  }
}

function setChunkData(key, value) {
  var suspend_data = get_suspend_data();
  suspend_data[key] = value;
  set_suspend_data(suspend_data);
}
function getChunkData(key) {
  var suspend_data = get_suspend_data();
  return suspend_data[key];
}
function carregaTempo() {
  if (SCORM_ACTIVE) {

    var SDD = window.parent;
    var time = SDD.GetPreviouslyAccumulatedTime() + SDD.GetSessionAccumulatedTime();

  } else {
    time = 0;
  }
  segundos = Math.round(time / 1000);
  $("#tempo").html(formataTempo(segundos));
  iniciaTempo();
}

function formataTempo(segs) {
  var min = 0, hr = 0;

  segs = Math.round(segs);

  while (segs >= 60) {
    if (segs >= 60) {
      segs = segs - 60;
      min = min + 1;
    }
  }

  while (min >= 60) {
    if (min >= 60) {
      min = min - 60;
      hr = hr + 1;
    }
  }

  if (hr < 10) {
    hr = "0" + hr;
  }
  if (min < 10) {
    min = "0" + min;
  }
  if (segs < 10) {
    segs = "0" + segs;
  }
  fin = hr + ":" + min + ":" + segs;

  return fin;
}

function contaTempo() {
  segundos++;
  $("#tempo").html(formataTempo(segundos));
}

function iniciaTempo() {
  interval = setInterval(contaTempo, 1000);
}

function RecordTrueFalseInteraction(strID, blnResponse, blnCorrect, blnCorrectResponse, strDescription, intWeighting, intLatency, strLearningObjectiveID) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    setChunkData('result_' + strID, blnCorrect);
    SDD.RecordTrueFalseInteraction(strID, blnResponse, blnCorrect, blnCorrectResponse, strDescription, intWeighting, intLatency, strLearningObjectiveID);
  }
}

function RecordMultipleChoiceInteraction(strID, response, blnCorrect, correctResponse, strDescription, intWeighting, intLatency, strLearningObjectiveID) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    setChunkData('result_' + strID, blnCorrect);
    SDD.RecordMultipleChoiceInteraction(strID, response, blnCorrect, correctResponse, strDescription, intWeighting, intLatency, strLearningObjectiveID);
    SDD.CommitData();
  }
}

function GetInteractionLearnerResponses(strInteractionID) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    return SDD.GetInteractionLearnerResponses(strInteractionID);
  }
  return null;
}

function GetInteractionResult(strInteractionID) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    return SDD.GetInteractionResult(strInteractionID);
  }
  return null;
}

function CreateMatchingResponse(pattern) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    return SDD.CreateMatchingResponse(pattern);
  }
  return null;
}

function RecordMatchingInteraction(strID, response, blnCorrect, correctResponse, strDescription, intWeighting, intLatency, strLearningObjectiveID) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    setChunkData('result_' + strID, blnCorrect);
    SDD.RecordMatchingInteraction(strID, response, blnCorrect, correctResponse, strDescription, intWeighting, intLatency, strLearningObjectiveID);
    SDD.CommitData();
  }
}

function GetObjectiveStatus(strObjectiveID) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    return SDD.GetObjectiveStatus(strObjectiveID);
  }
  return LESSON_STATUS_NOT_ATTEMPTED; //LESSON_STATUS_NOT_ATTEMPTED
}

function SetObjectiveStatus(strObjectiveID, Lesson_Status) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    SDD.SetObjectiveStatus(strObjectiveID, Lesson_Status);
    SDD.CommitData();
  }
}

function SetObjectiveScore(strObjectiveID, intScore, intMaxScore, intMinScore) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    SDD.SetObjectiveScore(strObjectiveID, intScore, intMaxScore, intMinScore);
    SDD.CommitData();
  }
}

function setObjectiveScoreBasedOnInteractions(strObjectiveID, strInteractionId, qtd, geral) {
  var atividades = [];
  var acertos = 0;
  for (i = 1; i <= qtd; i++) {
    result = getChunkData('result_' + strInteractionId + '' + i);
    if (result) {
      atividades.push('correct');
      acertos++;
    } else {
      atividades.push('incorrect');
    }
  }
  SetObjectiveScore(strObjectiveID, acertos, qtd, 0);
  SetObjectiveStatus(strObjectiveID, LESSON_STATUS_PASSED);
  if (geral) {
    SetPassed();
    SetScore(acertos, qtd, 0);
  }
  return atividades;
}
function resetPosTesteChunk(strInteractionId, qtd) {
  setChunkData('posTeste', false);
  for (i = 1; i <= qtd; i++) {
    setChunkData(strInteractionId + '' + i, false);
  }
}

function SetPassed() {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    SDD.SetPassed();
    SDD.CommitData();
  }
}

function SetScore(intScore, intMaxScore, intMinScore) {
  if (SCORM_ACTIVE) {
    var SDD = window.parent;
    SDD.SetScore(intScore, intMaxScore, intMinScore);
    SDD.CommitData();
  }
}

