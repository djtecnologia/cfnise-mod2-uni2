function resize_fonts(espaco) {
  ow = window.outerWidth;
  oh = window.outerHeight;

  var scw = espaco | $("#viewport").width();
  $('html, body').css('font-size', scw / 115 + 'pt');
}

function resize_viewport() {
  var zoom = document.body.style.zoom;
  if (!zoom) {
    zoom = '100%';
  }
  zoom = parseInt(zoom.replace('%', '')) / 100.0;
  ratio = 2.67;
  wh = window.innerHeight * 0.99 - 180;
  ww = window.innerWidth * 0.99 - 10;
  ht = wh;
  wt = ht * ratio;
  if (ww < wt) {
    wt = ww;
    ht = wt / ratio;
  }

  ht = (ht + 180 ) * zoom;
  wt = wt * zoom;

  margin_vert = (window.innerHeight - (ht)) / 2.0;
  $("body").css('margin-top', (margin_vert) + 'px');
  $("body").css('height', (ht ) + 'px');
  $("body").css('width', (wt) + 'px');

  setTimeout(function () {
    resize_fonts();

  }, 500);
}
$(window).on("resize", function () {
  setTimeout(function () {
    resize_viewport();
  }, 300);
});
$(document).ready(resize_viewport);