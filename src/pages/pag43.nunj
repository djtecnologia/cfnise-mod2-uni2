<!-- index.nunj -->
{% set bg_img_url='img/fundo1.jpg' %}
{% extends "T057.002.nunj" %}

 {% block titulo %}
   Acentuação Gráfica de Palavras Paroxítonas
 {% endblock %}

 {% block src_imagem %}
   img/img-04.png
 {% endblock %}

 {% block texto_1 %}
   As paroxítonas normalmente não são acentuadas e representam a maioria das palavras da língua portuguesa.
   <br/>
   Com o novo Acordo, algumas regras foram mantidas e outras alteradas. A perda do acento gráfico não implica, contudo, alteração de pronúncia.
 {% endblock %}

 {% block texto_2 %}
   As regras são as seguintes:
   <br/>
   <br/>

   <ul style="padding-left: 4%;">
     <li>são acentuadas as palavras paroxítonas terminadas em <strong>-l, -n,-r, -x, -ps, -ã(s), -ão(s), -ei(s), -i(s),
         -om(s), -on(s), -um, - uns, -us</strong> e pelos ditongos orais crescentes (proparoxítonas aparentes), como
       <strong>-ea, -eo, -ia, -ie, -io, -oa, -ua, -uo: agradável, fácil, éden, hífen, açúcar, caráter, tórax, xérox,
         bíceps, fórceps, órfã, órgão, jóquei, fáceis, táxi, oásis, rádom, iândom, elétron, prótons, álbum, fóruns,
         vírus, húmus; náusea, etéreo, glória, série, lírio, mágoa, língua, vácuo;</strong></li>
   </ul>
 {% endblock %}

 {% block texto_3 %}
   <ul style="padding-left: 4%;">
     <li>mantém-se acentuada a forma verbal <strong>pôde</strong> (3.<sup>a</sup> pessoa do singular do pretérito perfeito do
       indicativo) para se distinguir de <strong>pode</strong> (3.<sup>a</sup> pessoa do singular do presente do indicativo);
     </li>

     <li>perdem o acento gráfico os ditongos abertos <strong>-ei</strong> e <strong>-oi</strong> da sílaba tônica das
       palavras paroxítonas: <strong>assembleia</strong>, <strong>ideia</strong>, <strong>heroico</strong>, <strong>jiboia</strong>
       (mas atenção para a necessidade do acento em palavras que, mesmo se incluindo neste caso, se enquadram em regras
       de acentuação, como <strong>blêizer</strong>, <strong>destróier</strong> e <strong>Méier</strong>, porque são
       paroxítonas terminada sem <strong>-r</strong>);
     </li>
   </ul>
 {% endblock %}

 {% block texto_4 %}
   <ul style="padding-left: 4%;">
     <li>perde o acento gráfico o hiato <strong>ee</strong> das formas verbais paroxítonas das 3.<sup>a</sup> pessoas do plural do
       presente do indicativo ou do subjuntivo dos verbos <strong>crer, dar, ler e ver</strong> e seus derivados:
       <strong>creem, deem, leem, veem, descreem, releem, reveem;</strong></li>

     <li>perde o acento gráfico a vogal tônica fechada do hiato oo: <strong>enjoo, povoo, voo;</strong></li>
   </ul>
 {% endblock %}

 {% block texto_5 %}
   <ul style="padding-left: 4%;">
     <li>perdem o acento gráfico as palavras paroxítonas homógrafas homófonas ou não: <strong>para</strong>, verbo, e
       <strong>para</strong>, preposição, <strong>pela(s)</strong>, verbo e substantivo, e <strong>pela(s)</strong>,
       combinação de <strong>per</strong> e <strong>la(s)</strong>, <strong>pelo</strong> (verbo), <strong>pelo</strong>(s),
       substantivo, e <strong>pelo</strong>(s), combinação de <strong>per</strong> e <strong>lo</strong>(s), <strong>pera</strong>,
       substantivo, e <strong>pera</strong> (preposição antiga), <strong>polo</strong>(s), substantivo, e
       <strong>polo</strong>(s), combinação antiga e popular de <strong>por</strong> e <strong>lo(s)</strong>; o
       substantivo forma pode ser grafado fôrma para evitar ambiguidade com forma, 3.<sup>a</sup> pessoa do singular do presente do
       indicativo ou 2.<sup>a</sup> pessoa do singular do imperativo afirmativo do verbo formar.
     </li>
   </ul>
 {% endblock %}

 {% block num_nav_box %}
   4
 {% endblock %}

 {% block texto_destaque %}
   Clique no destaque
 {% endblock %}